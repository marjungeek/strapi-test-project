"use strict";

module.exports = {
  async priceUpdate(id, discount) {
    /* find the activity by id */
    const activitity = await strapi.query("activities").findOne(id);
    /* if no activity found */
    if (!activitity) return false;

    /* deduct the discount to the actual price */
    const discountedAmount = (discount / 100) * activitity.Price;

    return await strapi
      .query("activities")
      .update({ id: id }, { Price: activitity.Price - discountedAmount });
  },
};
