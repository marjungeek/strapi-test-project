"use strict";

const { parseMultipartData, sanitizeEntity } = require("strapi-utils");
const Filter = require("bad-words");
const filter = new Filter();

module.exports = {
  /* Update activity price */
  price_update: async (ctx) => {
    const { id } = ctx.params;
    const data = ctx.request.body;

    const result = await strapi.services.activities.priceUpdate(
      { id },
      data.Discount
    );
    return ctx.send(result);
  },
  /* Create new activity with sending email */
  create: async (ctx) => {
    let entity;
    if (ctx.is("multipart")) {
      const { data, files } = parseMultipartData(ctx);
      entity = await strapi.services.activities.create(data, { files });
    } else {
      entity = await strapi.services.activities.create(ctx.request.body);
    }
    //sanitize the entity
    entry = sanitizeEntity(entity, { model: strapi.models.activities });

    if (entry.Description !== filter.clean(entry.Description)) {
      // send an email by using the email plugin
      await strapi.plugins["email"].services.email.send({
        to: "info@mallocard.es",
        from: "new-activity@test.com",
        subject: "New Activity Created",
        text: `This is a sample email.`,
      });
    }

    return entry;
  },
};
