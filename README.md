# Strapi application Test

This application and database are both running on top of docker as container.
Make sure your docker is working fine to your machine.


### Dependencies
1. Installed docker on your machine
2. Installed NPM on your machine

### Step by step to run this application
1. Go to root directory and run 
    ***npm install***
	
2. Execute this command below
    ***docker-compose up -d***
	
3. Wait for the container and check if its running (this might take a little bit of time)
   *You can check it via cli or docker desktop.*
   
4. Import database_dump.sql to MariaDB that is running on container, Please see DB credential below

### NOTE: Please restart container multiple times if the there is some issues occurs on it.

### **DATABASE CREDENTIAL**
**HOST**: 127.0.0.1:3306
**MYSQL_DATABASE** :  db_strapi 
**MYSQL_USER** :           my_user
**MYSQL_PASSWORD** : my_password 

### Application
**URL:** localhost:8080/admin
**username:** marjunvillegas
**password:** Test12345