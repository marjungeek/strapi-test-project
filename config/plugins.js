module.exports = ({ env }) => ({
  email: {
    provider: "sendmail",
    settings: {
      defaultFrom: "marjunvillegas.geek@gmail.com",
      defaultReplyTo: "marjunvillegas@gmail.com",
    },
  },
});
